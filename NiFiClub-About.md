Project: NiFi Club
-----

Innovative universal NFT platform. Offers unparalleled range of proprietory contracts:

A. Tokens
- single edition token (plain vanilla token)
- limited edition token (series of identical tokens)
- single endorsable token (token capable of being endorsed)
- seal (special endorsing token)
- forever (sets of 4 of endorsable tokens)

B. Transactions
- auction (classic english auction with minimal price and bid steps)
- bid (guaranteed execution non-restricted bid)
- ask (fixed-price sale offer)
- endorsement (unique transaction of tokens cross-reference; purpose - certification / qualification of tokens)

Environment
- staging https://dev.nifi.club
- beta https://beta.nifi.club

Blockchains
- Everscale https://everscale.network
- Binance Smart Chain https://binance.com

Stack
- docker, node.js, next.js, MongoDB, GraphQL (Realm), Apollo, react.js
- integrations: eversdk.js, web3.js, Pinata, Contentful, Airtable

Code
- private https://gitlab.com/nifi-team
- public https://gitlab.com/nifi-public

Legal
- NiFi Club LLC, USA

Team
- Alexander Zvezdin (telegram @azvezdin)
